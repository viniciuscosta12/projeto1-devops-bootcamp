#!/bin/bash

echo "Atualizando o servidor..."

sudo apt-get update
sudo apt-get upgrate -y

echo "Instalando docker..."

curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh

echo "Instalando gitlab-runner..."

curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash

sudo apt-get install gitlab-runner